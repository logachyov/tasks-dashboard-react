import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './styles/app.scss';
import Header from "./components/Header";
import Home from "./routes/AugmentedExperiencesList";
import AssetsList from "./routes/AssetsList";
import ObjectView from "./routes/ObjectView";
import Sessions from "./routes/Sessions";
import Meetings from "./routes/Meetings";
import CreateEvent from "./routes/CreateEvent";
import Documents from "./routes/Documents";
import {Provider} from "react-redux";
import configureStore from './store/configureStore';
import {PersistGate} from 'redux-persist/lib/integration/react';
import {persistStore} from 'redux-persist';
import Menu from "./components/Menu"
import Sidebar from "./components/Sidebar"

const store = configureStore();
const persistor = persistStore(store);
function App() {
  return (
      <Provider store={store}>
          {/*<PersistGate persistor={persistor}>*/}
              <Router>
                  <div className="App">
                      <div className="container-fluid container-white">
                          <div className="row">
                              <div className="col-md-1 container-white">
                                  <Menu/>
                              </div>
                              <div className="col-md-9 container-grey">
                                  <main>
                                      {/*<Route exact path="/" component={Home} />*/}
                                      {/*<Route exact path="/assets-list" component={AssetsList} />*/}
                                      {/*<Route exact path="/object-view" component={ObjectView} />*/}
                                      {/*<Route exact path="/training-images" component={TrainingImages} />*/}
                                      <Route exact path="/sessions" component={Sessions} />
                                      <Route exact path="/meetings" component={Meetings} />
                                      <Route exact path="/meetings/createevent" component={CreateEvent} />
                                      <Route exact path="/documents" component={Documents} />
                                      <Route exact path="/3" component={Sessions} />
                                      <Route exact path="/4" component={Sessions} />
                                      <Route exact path="/5" component={Sessions} />
                                      <Route exact path="/6" component={Sessions} />
                                  </main>
                              </div>
                              <div className="col-md-2 container-white">
                                  <Sidebar/>
                              </div>
                          </div>
                      </div>
                  </div>
              </Router>
          {/*</PersistGate>*/}
      </Provider>

  );
}

export default App;
