import { combineReducers } from 'redux';
// import user from './user';
import objects from './objects';

const rootReducer = combineReducers({
    // user,
    objects
});

export default rootReducer;
