export const API_URL = 'http://localhost:4000',
    FETCH_GET_OBJECTS_PATH = API_URL + '/objects';


export function fetchGetItems() {
    return fetch(FETCH_GET_OBJECTS_PATH)
}