import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import 'rc-input-number/assets/index.css';
import InputNumber from 'rc-input-number';
import moment from 'moment'
import { AreaChart, Area, Tooltip, ResponsiveContainer} from 'recharts';


class InputHour extends React.Component {
    state = {
        disabled: false,
        readOnly: false,
        value: moment("01:00", "HH:mm"),
    };
    onChange = (value) => {
        console.log('onChange:', value);
        this.setState({ value });
    }
    toggleDisabled = () => {
        this.setState({
            disabled: !this.state.disabled,
        });
    }
    toggleReadOnly = () => {
        this.setState({
            readOnly: !this.state.readOnly,
        });
    }
    render() {
        const upHandler = (<div style={{ color: '#808FA3' }}>+</div>);
        const downHandler = (<div style={{ color: '#808FA3' }}>-</div>);
        /*console.log(moment('10:00', 'HH:mm').format("hA") )*/
        return (
            <div>
                <InputNumber
                    aria-label="Input hour"
                    min={1}
                    max={12}
                    value={this.state.value}
                    style={{ width: 88, height: 45 }}
                    readOnly={this.state.readOnly}
                    onChange={this.onChange}
                    disabled={this.state.disabled}
                    upHandler={upHandler}
                    downHandler={downHandler}
                    formatter={value => `${value}pm`}
                    /*formatter={value => moment().format("ha")}*/
                    parser={value => value.replace()}
                />
            </div>
        )
    }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => bindActionCreators({
},dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(InputHour);

