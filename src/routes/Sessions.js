import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import avatar from '../images/avatar1.png'
import avatar2 from '../images/avatar2.png'
import avatar3 from '../images/avatar3.png'
import avatar4 from '../images/avatar4.png'
import avatar5 from '../images/avatar5.png'
import avatar6 from '../images/avatar6.png'
import { Link } from 'react-router-dom';
import { AreaChart, Area, Tooltip, ResponsiveContainer} from 'recharts';
import SimpleBar from 'simplebar-react';
import 'simplebar/dist/simplebar.min.css';

class Sessions extends React.Component {

    render() {
        const data = [
            {
                name: 'Mon', uv: 400, pv: 240, amt: 240,
            },
            {
                name: 'Tue', uv: 300, pv: 139, amt: 221,
            },
            {
                name: 'Wed', uv: 200, pv: 980, amt: 229,
            },
            {
                name: 'Thu', uv: 278, pv: 390, amt: 200,
            },
            {
                name: 'Fri', uv: 189, pv: 480, amt: 218,
            },
            {
                name: 'Sat', uv: 239, pv: 380, amt: 250,
            },
            {
                name: 'Sun', uv: 349, pv: 430, amt: 210,
            },
        ];
        return (
            <div>
                <div className="row header-title">
                    <div className="col-md-6">
                        <div className="sessions-header">
                            <h1 className="all-title sessions-title">Sessions</h1>
                            <Link to={"/"} role="button" className="btn sessions-header__export-btn">
                                <i className="fas fa-download sessions-header__export-icon"/>
                                <span className="sessions-header__export-text">Export</span>
                            </Link>
                        </div>
                    </div>
                    <div className="col-md-6 d-flex flex-row justify-content-end">
                        <div className="btn-group btn-group-toggle sessions-header__view" data-toggle="buttons">
                            <label className="btn sessions-header__view-cards active">
                                <input type="radio" name="view" id="view1" autoComplete="off" checked/>
                                <i className="fas fa-qrcode sessions-header__view-icon"/>
                                <span className="sessions-header__view-text">Cards</span>
                            </label>
                            <label className="btn sessions-header__view-cards">
                                <input type="radio" name="view" id="view2" autoComplete="off"/>
                                <i className="fas fa-bars sessions-header__view-icon"/>
                                <span className="sessions-header__view-text">Table</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div className="row element-margin">
                    <div className="col-md-4">
                        <div className="sessions-card colour-white">
                            <div className="sessions-card__title">
                                <span className="sessions-card__title-head">Number of sessions</span>
                                <i className="fas fa-ellipsis-h sessions-card__title-icon"/>
                            </div>
                            <span className="sessions-card__subtitle">
                                    <span className="sessions-card__subtitle-number">23</span>
                                    This month
                            </span>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="sessions-card colour-white">
                            <div className="sessions-card__title">
                                <span className="sessions-card__title-head">Users  who haven’t answered</span>
                                <i className="fas fa-ellipsis-h sessions-card__title-icon"/>
                            </div>
                            <span className="sessions-card__subtitle">
                                    <span className="sessions-card__subtitle-number">06</span>
                                    This month
                            </span>
                        </div>
                    </div>
                </div>
                <h3 className="date-title">
                    <span className="date-title__number">14</span><span className="date-title__ordinal">th</span> Tuesday
                </h3>
                <div className="row sessions-scroll">
                    <div className="col-md-4">
                        <div className="users-wrap colour-white">
                            <div className="media users-wrap__header">
                                <img src={avatar} className="users-wrap__header-img" alt=""/>
                                <div className="media-body users-wrap__header-content">
                                    <h5 className="content-title">Samuel Spencer</h5>
                                    <span className="content-mail">sasp@egament.com</span>
                                    <span className="content-position">Creative Director</span>
                                </div>
                            </div>
                            <div className="users-wrap__overall">
                                <div className="btn-group btn-group-toggle user-wrap__overall-btn" data-toggle="buttons">
                                    <label className="btn overall-btn active">
                                        <input type="radio" name="invites" id="invite" autoComplete="off" checked/>
                                        <span className="overall-btn__text">Invites</span>
                                        <span className="overall-btn__number">03</span>
                                    </label>
                                    <label className="btn overall-btn">
                                        <input type="radio" name="pendin" id="pending" autoComplete="off" checked/>
                                        <span className="overall-btn__text">Pending</span>
                                        <span className="overall-btn__number overall-btn__number-color">
                                            <i className="fas fa-circle overall-btn__number-icon"/>02</span>
                                    </label>
                                </div>
                                <div className="user-wrap__overall-activity">
                                    <span className="overall-activity__title">Overall activity</span>
                                    <ResponsiveContainer width={'100%'} height={50}>
                                        <AreaChart
                                            data={data}
                                            margin={{
                                                top: 10, right: -5, left: -5, bottom: 0,
                                            }}
                                        >
                                            <Tooltip />
                                            <defs>
                                                <linearGradient id="colorInvites" x1="0" y1="0" x2="0" y2="1">
                                                    <stop offset="-266.35%" stopColor="#00ACFF" stopOpacity={0.99} />
                                                    <stop offset="57.01%" stopColor={"rgb(0, 172, 255)"} stopOpacity={0.05} />
                                                </linearGradient>
                                            </defs>
                                            <Area type="monotone" dataKey="uv" strokeWidth={2} stroke="#00AAFF" fill={"url(#colorInvites)"} />
                                        </AreaChart>
                                    </ResponsiveContainer>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="users-wrap colour-white">
                            <div className="media users-wrap__header">
                                <img src={avatar2} className="users-wrap__header-img" alt=""/>
                                <div className="media-body users-wrap__header-content">
                                    <h5 className="content-title">Kit Laframboise</h5>
                                    <span className="content-mail">kila@egament.com</span>
                                    <span className="content-position">Data Analysit</span>
                                </div>
                            </div>
                            <div className="users-wrap__overall">
                                <div className="btn-group btn-group-toggle user-wrap__overall-btn" data-toggle="buttons">
                                    <label className="btn overall-btn active">
                                        <input type="radio" name="invites" id="invite" autoComplete="off" checked/>
                                        <span className="overall-btn__text">Invites</span>
                                        <span className="overall-btn__number">07</span>
                                    </label>
                                    <label className="btn overall-btn">
                                        <input type="radio" name="pendin" id="pending" autoComplete="off" checked/>
                                        <span className="overall-btn__text">Pending</span>
                                        <span className="overall-btn__number overall-btn__number-color">
                                            <i className="fas fa-circle overall-btn__number-icon"/>00</span>
                                    </label>
                                </div>
                                <div className="user-wrap__overall-activity">
                                    <span className="overall-activity__title">Overall activity</span>
                                    <ResponsiveContainer width={'100%'} height={50}>
                                        <AreaChart
                                            data={data}
                                            margin={{
                                                top: 10, right: -5, left: -5, bottom: 0,
                                            }}
                                        >
                                            <Tooltip />
                                            <defs>
                                                <linearGradient id="colorPending" x1="0" y1="0" x2="0" y2="1">
                                                    <stop offset="-395.43%" stopColor="#FC577A" stopOpacity={0.99} />
                                                    <stop offset="57.01%" stopColor={"rgb(252, 87, 122)"} stopOpacity={0.05} />
                                                </linearGradient>
                                            </defs>
                                            <Area type="monotone" dataKey="uv" strokeWidth={2} stroke="#FC577A" fill={"url(#colorPending)"} />
                                        </AreaChart>
                                    </ResponsiveContainer>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="users-wrap colour-white">
                            <div className="media users-wrap__header">
                                <img src={avatar3} className="users-wrap__header-img" alt=""/>
                                <div className="media-body users-wrap__header-content">
                                    <h5 className="content-title">Abagael Enno</h5>
                                    <span className="content-mail">anhu@egament.com</span>
                                    <span className="content-position">Product Manager</span>
                                </div>
                            </div>
                            <div className="users-wrap__overall">
                                <div className="btn-group btn-group-toggle user-wrap__overall-btn" data-toggle="buttons">
                                    <label className="btn overall-btn active">
                                        <input type="radio" name="invites" id="invite" autoComplete="off" checked/>
                                        <span className="overall-btn__text">Invites</span>
                                        <span className="overall-btn__number">03</span>
                                    </label>
                                    <label className="btn overall-btn">
                                        <input type="radio" name="pendin" id="pending" autoComplete="off" checked/>
                                        <span className="overall-btn__text">Pending</span>
                                        <span className="overall-btn__number overall-btn__number-color">
                                            <i className="fas fa-circle overall-btn__number-icon"/>02</span>
                                    </label>
                                </div>
                                <div className="user-wrap__overall-activity">
                                    <span className="overall-activity__title">Overall activity</span>
                                    <ResponsiveContainer width={'100%'} height={50}>
                                        <AreaChart
                                            data={data}
                                            margin={{
                                                top: 10, right: -5, left: -5, bottom: 0,
                                            }}
                                        >
                                            <Tooltip />
                                            <defs>
                                                <linearGradient id="colorPending" x1="0" y1="0" x2="0" y2="1">
                                                    <stop offset="-395.43%" stopColor="#FC577A" stopOpacity={0.99} />
                                                    <stop offset="57.01%" stopColor={"rgb(252, 87, 122)"} stopOpacity={0.05} />
                                                </linearGradient>
                                            </defs>
                                            <Area type="monotone" dataKey="uv" strokeWidth={2} stroke="#FC577A" fill={"url(#colorPending)"} />
                                        </AreaChart>
                                    </ResponsiveContainer>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="users-wrap colour-white">
                            <div className="media users-wrap__header">
                                <img src={avatar4} className="users-wrap__header-img" alt=""/>
                                <div className="media-body users-wrap__header-content">
                                    <h5 className="content-title">Amy Dettmering</h5>
                                    <span className="content-mail">amde@egament.com</span>
                                    <span className="content-position">Project Manager</span>
                                </div>
                            </div>
                            <div className="users-wrap__overall">
                                <div className="btn-group btn-group-toggle user-wrap__overall-btn" data-toggle="buttons">
                                    <label className="btn overall-btn active">
                                        <input type="radio" name="invites" id="invite" autoComplete="off" checked/>
                                        <span className="overall-btn__text">Invites</span>
                                        <span className="overall-btn__number">03</span>
                                    </label>
                                    <label className="btn overall-btn">
                                        <input type="radio" name="pendin" id="pending" autoComplete="off" checked/>
                                        <span className="overall-btn__text">Pending</span>
                                        <span className="overall-btn__number overall-btn__number-color">
                                            <i className="fas fa-circle overall-btn__number-icon"/>02</span>
                                    </label>
                                </div>
                                <div className="user-wrap__overall-activity">
                                    <span className="overall-activity__title">Overall activity</span>
                                    <ResponsiveContainer width={'100%'} height={50}>
                                        <AreaChart
                                            data={data}
                                            margin={{
                                                top: 10, right: -5, left: -5, bottom: 0,
                                            }}
                                        >
                                            <Tooltip />
                                            <defs>
                                                <linearGradient id="colorPending" x1="0" y1="0" x2="0" y2="1">
                                                    <stop offset="-395.43%" stopColor="#FC577A" stopOpacity={0.99} />
                                                    <stop offset="57.01%" stopColor={"rgb(252, 87, 122)"} stopOpacity={0.05} />
                                                </linearGradient>
                                            </defs>
                                            <Area type="monotone" dataKey="uv" strokeWidth={2} stroke="#FC577A" fill={"url(#colorPending)"} />
                                        </AreaChart>
                                    </ResponsiveContainer>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="users-wrap colour-white">
                            <div className="media users-wrap__header">
                                <img src={avatar5} className="users-wrap__header-img" alt=""/>
                                <div className="media-body users-wrap__header-content">
                                    <h5 className="content-title">Carl Durk</h5>
                                    <span className="content-mail">cadu@egament.com</span>
                                    <span className="content-position">UX Designer</span>
                                </div>
                            </div>
                            <div className="users-wrap__overall">
                                <div className="btn-group btn-group-toggle user-wrap__overall-btn" data-toggle="buttons">
                                    <label className="btn overall-btn active">
                                        <input type="radio" name="invites" id="invite" autoComplete="off" checked/>
                                        <span className="overall-btn__text">Invites</span>
                                        <span className="overall-btn__number">03</span>
                                    </label>
                                    <label className="btn overall-btn">
                                        <input type="radio" name="pendin" id="pending" autoComplete="off" checked/>
                                        <span className="overall-btn__text">Pending</span>
                                        <span className="overall-btn__number overall-btn__number-color">
                                            <i className="fas fa-circle overall-btn__number-icon"/>02</span>
                                    </label>
                                </div>
                                <div className="user-wrap__overall-activity">
                                    <span className="overall-activity__title">Overall activity</span>
                                    <ResponsiveContainer width={'100%'} height={50}>
                                        <AreaChart
                                            data={data}
                                            margin={{
                                                top: 10, right: -5, left: -5, bottom: 0,
                                            }}
                                        >
                                            <Tooltip />
                                            <defs>
                                                <linearGradient id="colorInvites" x1="0" y1="0" x2="0" y2="1">
                                                    <stop offset="-266.35%" stopColor="#00ACFF" stopOpacity={0.99} />
                                                    <stop offset="57.01%" stopColor={"rgb(0, 172, 255)"} stopOpacity={0.05} />
                                                </linearGradient>
                                            </defs>
                                            <Area type="monotone" dataKey="uv" strokeWidth={2} stroke="#00AAFF" fill={"url(#colorInvites)"} />
                                        </AreaChart>
                                    </ResponsiveContainer>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="users-wrap colour-white">
                            <div className="media users-wrap__header">
                                <img src={avatar6} className="users-wrap__header-img" alt=""/>
                                <div className="media-body users-wrap__header-content">
                                    <h5 className="content-title">Luciano Niswander</h5>
                                    <span className="content-mail">luni@egament.com</span>
                                    <span className="content-position">Full-stack Developer</span>
                                </div>
                            </div>
                            <div className="users-wrap__overall">
                                <div className="btn-group btn-group-toggle user-wrap__overall-btn" data-toggle="buttons">
                                    <label className="btn overall-btn active">
                                        <input type="radio" name="invites" id="invite" autoComplete="off" checked/>
                                        <span className="overall-btn__text">Invites</span>
                                        <span className="overall-btn__number">03</span>
                                    </label>
                                    <label className="btn overall-btn">
                                        <input type="radio" name="pendin" id="pending" autoComplete="off" checked/>
                                        <span className="overall-btn__text">Pending</span>
                                        <span className="overall-btn__number overall-btn__number-color">
                                            <i className="fas fa-circle overall-btn__number-icon"/>02</span>
                                    </label>
                                </div>
                                <div className="user-wrap__overall-activity">
                                    <span className="overall-activity__title">Overall activity</span>
                                    <ResponsiveContainer width={'100%'} height={50}>
                                        <AreaChart
                                            data={data}
                                            margin={{
                                                top: 10, right: -5, left: -5, bottom: 0,
                                            }}
                                        >
                                            <Tooltip />
                                            <defs>
                                                <linearGradient id="colorInvites" x1="0" y1="0" x2="0" y2="1">
                                                    <stop offset="-266.35%" stopColor="#00ACFF" stopOpacity={0.99} />
                                                    <stop offset="57.01%" stopColor={"rgb(0, 172, 255)"} stopOpacity={0.05} />
                                                </linearGradient>
                                            </defs>
                                            <Area type="monotone" dataKey="uv" strokeWidth={2} stroke="#00AAFF" fill={"url(#colorInvites)"} />
                                        </AreaChart>
                                    </ResponsiveContainer>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => bindActionCreators({
},dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Sessions);

