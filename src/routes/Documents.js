import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { AreaChart, Area, Tooltip, ResponsiveContainer} from 'recharts';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';
import classnames from 'classnames';
import ReactSVG from 'react-svg'
import Doc from '../images/files-icon/icon-doc.svg'
import Psd from '../images/files-icon/icon-psd.svg'
import Pdf from '../images/files-icon/icon-pdf.svg'
import Pptx from '../images/files-icon/icon-pptx.svg'
import folder from "../images/files-icon/folder-icon.svg";
import sharedIcon from "../images/shared-icon.svg";

class Documents extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            activeTab:1,
            activeTabFolder:1
        };
    }

    handleToggle = (activeTab,tabName) => {
        this.setState({
            [tabName]: activeTab
        })
    }

    render() {
        const {activeTab} = this.state;
        const {activeTabFolder} = this.state;

        return (
            <div>
                {/*Header*/}
                <div className="row header-title">
                    <div className="col-md-6">
                        <div className="documents-header">
                            <h1 className="all-title documents-title">Documents</h1>
                            <button className="btn documents-header__add-btn" type="button">
                                <span className="documents-header__add-text">+ Add files</span>
                            </button>
                        </div>
                    </div>
                    <div className="col-md-6 d-flex flex-row justify-content-end">
                        <div className="btn-group btn-group-toggle documents-header__view" data-toggle="buttons">
                            <label className="btn documents-header__view-cards active">
                                <input type="radio" name="view" id="viewDay" autoComplete="off" checked/>
                                <span className="documents-header__view-text">Hikoot app</span>
                            </label>
                            <label className="btn documents-header__view-cards">
                                <input type="radio" name="view" id="viewWeek" autoComplete="off"/>
                                <span className="documents-header__view-text">New bizz</span>
                            </label>
                            <label className="btn documents-header__view-cards">
                                <input type="radio" name="view" id="viewMonth" autoComplete="off"/>
                                <span className="documents-header__view-text">My files</span>
                            </label>
                        </div>
                    </div>
                </div>
                {/*Content*/}
                <div className="row documents-content">
                    {/*Favourites*/}
                    <div className="col-md-4">
                        <div className="favourites-item colour-white">
                            <span className="documents-title__header">Favourites</span>
                            {/*Navigation*/}
                            <Nav tabs className="favourites-item__tab">
                                <NavItem className="favourites-item__tab-nav">
                                    <NavLink
                                        className={classnames({ active: activeTab === '1' }, "tab-nav__link")}
                                        onClick={() => { this.handleToggle('1','activeTab'); }}
                                    >
                                        <ReactSVG className="icon-img__tab" src={folder} alt=""/>
                                        My files
                                        <i className="fas fa-ellipsis-v tab-nav__icon-dots"/>
                                    </NavLink>
                                </NavItem>
                                <NavItem className="favourites-item__tab-nav">
                                    <NavLink
                                        className={classnames({ active: activeTab === '2' }, "tab-nav__link")}
                                        onClick={() => { this.handleToggle('2','activeTab'); }}
                                    >
                                        <ReactSVG className="icon-img__tab" src={folder} alt=""/>
                                        Shared with me
                                        <i className="fas fa-ellipsis-v tab-nav__icon-dots"/>
                                    </NavLink>
                                </NavItem>
                                <NavItem className="favourites-item__tab-nav">
                                    <NavLink
                                        className={classnames({ active: activeTab === '3' }, "tab-nav__link")}
                                        onClick={() => { this.handleToggle('3','activeTab'); }}
                                    >
                                        <ReactSVG className="icon-img__tab" src={folder} alt=""/>
                                        Design resources
                                        <i className="fas fa-ellipsis-v tab-nav__icon-dots"/>
                                    </NavLink>
                                </NavItem>
                                <NavItem className="favourites-item__tab-nav">
                                    <NavLink
                                        className={classnames({ active: activeTab === '4' }, "tab-nav__link")}
                                        onClick={() => { this.handleToggle('4','activeTab'); }}
                                    >
                                        <ReactSVG className="icon-img__tab" src={folder} alt=""/>
                                        Daily reports
                                        <i className="fas fa-ellipsis-v tab-nav__icon-dots"/>
                                    </NavLink>
                                </NavItem>
                                <NavItem className="favourites-item__tab-nav">
                                    <NavLink
                                        className={classnames({ active: activeTab === '5' }, "tab-nav__link")}
                                        onClick={() => { this.handleToggle('5','activeTab'); }}
                                    >
                                        <ReactSVG className="icon-img__tab" src={folder} alt=""/>
                                        HAY
                                        <i className="fas fa-ellipsis-v tab-nav__icon-dots"/>
                                    </NavLink>
                                </NavItem>
                                <NavItem className="favourites-item__tab-nav">
                                    <NavLink
                                        className={classnames({ active: activeTab === '6' }, "tab-nav__link")}
                                        onClick={() => { this.handleToggle('6','activeTab'); }}
                                    >
                                        <ReactSVG className="icon-img__tab" src={sharedIcon} alt=""/>
                                        Amy’s inspiration
                                        <i className="fas fa-ellipsis-v tab-nav__icon-dots"/>
                                    </NavLink>
                                </NavItem>
                            </Nav>
                        </div>
                    </div>
                    {/*Folder and files*/}
                    <div className="col-md-8">
                        <TabContent activeTab={activeTab} className="folders-item colour-white">
                            {/*My files*/}
                            <TabPane tabId="1" className="folders-item__tabpane">
                                <span className="documents-title__header folders-title__header">Folders</span>
                                {/*Navigation folder*/}
                                <Nav tabs className="folders-item__tab">
                                    <NavItem className="folders-item__tab-nav">
                                        <NavLink
                                            className={classnames({ active: activeTabFolder === '1' }, "folders-nav__link")}
                                            onClick={() => { this.handleToggle('1','activeTabFolder'); }}
                                        >
                                            <ReactSVG className="folders-nav__icon-folder" src={folder} alt=""/>
                                            Base elements
                                        </NavLink>
                                    </NavItem>
                                    <NavItem className="folders-item__tab-nav">
                                        <NavLink
                                            className={classnames({ active: activeTabFolder === '2' }, "folders-nav__link")}
                                            onClick={() => { this.handleToggle('2','activeTabFolder'); }}
                                        >
                                            <ReactSVG className="folders-nav__icon-folder" src={sharedIcon} alt=""/>
                                            Inspiration
                                        </NavLink>
                                    </NavItem>
                                </Nav>
                                <span className="documents-title__header files-title__header">Files</span>
                                {/*Files*/}
                                <TabContent activeTab={activeTabFolder} className="files-item">
                                    <TabPane tabId="1" className="files-item_tab-content">
                                        <div className="files-item_content">
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Doc} className="content-docs__img" alt="Doc"/>
                                                <span className="content-docs__text">Hikoot_Concept</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pdf} className="content-docs__img" alt="Pdf"/>
                                                <span className="content-docs__text">Hikoot_Proposal</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Psd} className="content-docs__img" alt="Psd"/>
                                                <span className="content-docs__text">Hikoot_Desktop</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pptx} className="content-docs__img" alt="Pptx"/>
                                                <span className="content-docs__text">Hikoot_Presentation</span>
                                            </Link>
                                        </div>
                                    </TabPane>
                                    <TabPane tabId="2" className="files-item_tab-content">
                                        <div className="files-item_content">
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Psd} className="content-docs__img" alt="Psd"/>
                                                <span className="content-docs__text">Hikoot_Desktop</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pptx} className="content-docs__img" alt="Pptx"/>
                                                <span className="content-docs__text">Hikoot_Presentation</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Doc} className="content-docs__img" alt="Doc"/>
                                                <span className="content-docs__text">Hikoot_Concept</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pdf} className="content-docs__img" alt="Pdf"/>
                                                <span className="content-docs__text">Hikoot_Proposal</span>
                                            </Link>
                                        </div>
                                    </TabPane>
                                </TabContent>
                            </TabPane>
                            {/*Shared with me*/}
                            <TabPane tabId="2" className="folders-item__tabpane">
                                <span className="documents-title__header folders-title__header">Folders</span>
                                {/*Navigation folder*/}
                                <Nav tabs className="folders-item__tab">
                                    <NavItem className="folders-item__tab-nav">
                                        <NavLink
                                            className={classnames({ active: activeTabFolder === '1' }, "folders-nav__link")}
                                            onClick={() => { this.handleToggle('1','activeTabFolder'); }}
                                        >
                                            <ReactSVG className="folders-nav__icon-folder" src={folder} alt=""/>
                                            2Base elements
                                        </NavLink>
                                    </NavItem>
                                    <NavItem className="folders-item__tab-nav">
                                        <NavLink
                                            className={classnames({ active: activeTabFolder === '2' }, "folders-nav__link")}
                                            onClick={() => { this.handleToggle('2','activeTabFolder'); }}
                                        >
                                            <ReactSVG className="folders-nav__icon-folder" src={sharedIcon} alt=""/>
                                            2Inspiration
                                        </NavLink>
                                    </NavItem>
                                </Nav>
                                <span className="documents-title__header files-title__header">Files</span>
                                {/*Files*/}
                                <TabContent activeTab={activeTabFolder} className="files-item">
                                    <TabPane tabId="1" className="files-item_tab-content">
                                        <div className="files-item_content">
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Doc} className="content-docs__img" alt="Doc"/>
                                                <span className="content-docs__text">Hikoot_Concept</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pdf} className="content-docs__img" alt="Pdf"/>
                                                <span className="content-docs__text">Hikoot_Proposal</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Psd} className="content-docs__img" alt="Psd"/>
                                                <span className="content-docs__text">Hikoot_Desktop</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pptx} className="content-docs__img" alt="Pptx"/>
                                                <span className="content-docs__text">Hikoot_Presentation</span>
                                            </Link>
                                        </div>
                                    </TabPane>
                                    <TabPane tabId="2" className="files-item_tab-content">
                                        <div className="files-item_content">
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Psd} className="content-docs__img" alt="Psd"/>
                                                <span className="content-docs__text">Hikoot_Desktop</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pptx} className="content-docs__img" alt="Pptx"/>
                                                <span className="content-docs__text">Hikoot_Presentation</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Doc} className="content-docs__img" alt="Doc"/>
                                                <span className="content-docs__text">Hikoot_Concept</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pdf} className="content-docs__img" alt="Pdf"/>
                                                <span className="content-docs__text">Hikoot_Proposal</span>
                                            </Link>
                                        </div>
                                    </TabPane>
                                </TabContent>
                            </TabPane>
                            {/*Design resources*/}
                            <TabPane tabId="3" className="folders-item__tabpane">
                                <span className="documents-title__header folders-title__header">Folders</span>
                                {/*Navigation folder*/}
                                <Nav tabs className="folders-item__tab">
                                    <NavItem className="folders-item__tab-nav">
                                        <NavLink
                                            className={classnames({ active: activeTabFolder === '1' }, "folders-nav__link")}
                                            onClick={() => { this.handleToggle('1','activeTabFolder'); }}
                                        >
                                            <ReactSVG className="folders-nav__icon-folder" src={folder} alt=""/>
                                            3Base elements
                                        </NavLink>
                                    </NavItem>
                                    <NavItem className="folders-item__tab-nav">
                                        <NavLink
                                            className={classnames({ active: activeTabFolder === '2' }, "folders-nav__link")}
                                            onClick={() => { this.handleToggle('2','activeTabFolder'); }}
                                        >
                                            <ReactSVG className="folders-nav__icon-folder" src={sharedIcon} alt=""/>
                                            3Inspiration
                                        </NavLink>
                                    </NavItem>
                                </Nav>
                                <span className="documents-title__header files-title__header">Files</span>
                                {/*Files*/}
                                <TabContent activeTab={activeTabFolder} className="files-item">
                                    <TabPane tabId="1" className="files-item_tab-content">
                                        <div className="files-item_content">
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Doc} className="content-docs__img" alt="Doc"/>
                                                <span className="content-docs__text">Hikoot_Concept</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pdf} className="content-docs__img" alt="Pdf"/>
                                                <span className="content-docs__text">Hikoot_Proposal</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Psd} className="content-docs__img" alt="Psd"/>
                                                <span className="content-docs__text">Hikoot_Desktop</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pptx} className="content-docs__img" alt="Pptx"/>
                                                <span className="content-docs__text">Hikoot_Presentation</span>
                                            </Link>
                                        </div>
                                    </TabPane>
                                    <TabPane tabId="2" className="files-item_tab-content">
                                        <div className="files-item_content">
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Psd} className="content-docs__img" alt="Psd"/>
                                                <span className="content-docs__text">Hikoot_Desktop</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pptx} className="content-docs__img" alt="Pptx"/>
                                                <span className="content-docs__text">Hikoot_Presentation</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Doc} className="content-docs__img" alt="Doc"/>
                                                <span className="content-docs__text">Hikoot_Concept</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pdf} className="content-docs__img" alt="Pdf"/>
                                                <span className="content-docs__text">Hikoot_Proposal</span>
                                            </Link>
                                        </div>
                                    </TabPane>
                                </TabContent>
                            </TabPane>
                            {/*Daily reports*/}
                            <TabPane tabId="4" className="folders-item__tabpane">
                                <span className="documents-title__header folders-title__header">Folders</span>
                                {/*Navigation folder*/}
                                <Nav tabs className="folders-item__tab">
                                    <NavItem className="folders-item__tab-nav">
                                        <NavLink
                                            className={classnames({ active: activeTabFolder === '1' }, "folders-nav__link")}
                                            onClick={() => { this.handleToggle('1','activeTabFolder'); }}
                                        >
                                            <ReactSVG className="folders-nav__icon-folder" src={folder} alt=""/>
                                            4Base elements
                                        </NavLink>
                                    </NavItem>
                                    <NavItem className="folders-item__tab-nav">
                                        <NavLink
                                            className={classnames({ active: activeTabFolder === '2' }, "folders-nav__link")}
                                            onClick={() => { this.handleToggle('2','activeTabFolder'); }}
                                        >
                                            <ReactSVG className="folders-nav__icon-folder" src={sharedIcon} alt=""/>
                                            4Inspiration
                                        </NavLink>
                                    </NavItem>
                                </Nav>
                                <span className="documents-title__header files-title__header">Files</span>
                                {/*Files*/}
                                <TabContent activeTab={activeTabFolder} className="files-item">
                                    <TabPane tabId="1" className="files-item_tab-content">
                                        <div className="files-item_content">
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Doc} className="content-docs__img" alt="Doc"/>
                                                <span className="content-docs__text">Hikoot_Concept</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pdf} className="content-docs__img" alt="Pdf"/>
                                                <span className="content-docs__text">Hikoot_Proposal</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Psd} className="content-docs__img" alt="Psd"/>
                                                <span className="content-docs__text">Hikoot_Desktop</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pptx} className="content-docs__img" alt="Pptx"/>
                                                <span className="content-docs__text">Hikoot_Presentation</span>
                                            </Link>
                                        </div>
                                    </TabPane>
                                    <TabPane tabId="2" className="files-item_tab-content">
                                        <div className="files-item_content">
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Psd} className="content-docs__img" alt="Psd"/>
                                                <span className="content-docs__text">Hikoot_Desktop</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pptx} className="content-docs__img" alt="Pptx"/>
                                                <span className="content-docs__text">Hikoot_Presentation</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Doc} className="content-docs__img" alt="Doc"/>
                                                <span className="content-docs__text">Hikoot_Concept</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pdf} className="content-docs__img" alt="Pdf"/>
                                                <span className="content-docs__text">Hikoot_Proposal</span>
                                            </Link>
                                        </div>
                                    </TabPane>
                                </TabContent>
                            </TabPane>
                            {/*HAY*/}
                            <TabPane tabId="5" className="folders-item__tabpane">
                                <span className="documents-title__header folders-title__header">Folders</span>
                                {/*Navigation folder*/}
                                <Nav tabs className="folders-item__tab">
                                    <NavItem className="folders-item__tab-nav">
                                        <NavLink
                                            className={classnames({ active: activeTabFolder === '1' }, "folders-nav__link")}
                                            onClick={() => { this.handleToggle('1','activeTabFolder'); }}
                                        >
                                            <ReactSVG className="folders-nav__icon-folder" src={folder} alt=""/>
                                            5Base elements
                                        </NavLink>
                                    </NavItem>
                                    <NavItem className="folders-item__tab-nav">
                                        <NavLink
                                            className={classnames({ active: activeTabFolder === '2' }, "folders-nav__link")}
                                            onClick={() => { this.handleToggle('2','activeTabFolder'); }}
                                        >
                                            <ReactSVG className="folders-nav__icon-folder" src={sharedIcon} alt=""/>
                                            5Inspiration
                                        </NavLink>
                                    </NavItem>
                                </Nav>
                                <span className="documents-title__header files-title__header">Files</span>
                                {/*Files*/}
                                <TabContent activeTab={activeTabFolder} className="files-item">
                                    <TabPane tabId="1" className="files-item_tab-content">
                                        <div className="files-item_content">
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Doc} className="content-docs__img" alt="Doc"/>
                                                <span className="content-docs__text">Hikoot_Concept</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pdf} className="content-docs__img" alt="Pdf"/>
                                                <span className="content-docs__text">Hikoot_Proposal</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Psd} className="content-docs__img" alt="Psd"/>
                                                <span className="content-docs__text">Hikoot_Desktop</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pptx} className="content-docs__img" alt="Pptx"/>
                                                <span className="content-docs__text">Hikoot_Presentation</span>
                                            </Link>
                                        </div>
                                    </TabPane>
                                    <TabPane tabId="2" className="files-item_tab-content">
                                        <div className="files-item_content">
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Psd} className="content-docs__img" alt="Psd"/>
                                                <span className="content-docs__text">Hikoot_Desktop</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pptx} className="content-docs__img" alt="Pptx"/>
                                                <span className="content-docs__text">Hikoot_Presentation</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Doc} className="content-docs__img" alt="Doc"/>
                                                <span className="content-docs__text">Hikoot_Concept</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pdf} className="content-docs__img" alt="Pdf"/>
                                                <span className="content-docs__text">Hikoot_Proposal</span>
                                            </Link>
                                        </div>
                                    </TabPane>
                                </TabContent>
                            </TabPane>
                            {/*Amy’s inspiration*/}
                            <TabPane tabId="6" className="folders-item__tabpane">
                                <span className="documents-title__header folders-title__header">Folders</span>
                                {/*Navigation folder*/}
                                <Nav tabs className="folders-item__tab">
                                    <NavItem className="folders-item__tab-nav">
                                        <NavLink
                                            className={classnames({ active: activeTabFolder === '1' }, "folders-nav__link")}
                                            onClick={() => { this.handleToggle('1','activeTabFolder'); }}
                                        >
                                            <ReactSVG className="folders-nav__icon-folder" src={folder} alt=""/>
                                            6Base elements
                                        </NavLink>
                                    </NavItem>
                                    <NavItem className="folders-item__tab-nav">
                                        <NavLink
                                            className={classnames({ active: activeTabFolder === '2' }, "folders-nav__link")}
                                            onClick={() => { this.handleToggle('2','activeTabFolder'); }}
                                        >
                                            <ReactSVG className="folders-nav__icon-folder" src={sharedIcon} alt=""/>
                                            6Inspiration
                                        </NavLink>
                                    </NavItem>
                                </Nav>
                                <span className="documents-title__header files-title__header">Files</span>
                                {/*Files*/}
                                <TabContent activeTab={activeTabFolder} className="files-item">
                                    <TabPane tabId="1" className="files-item_tab-content">
                                        <div className="files-item_content">
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Doc} className="content-docs__img" alt="Doc"/>
                                                <span className="content-docs__text">Hikoot_Concept</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pdf} className="content-docs__img" alt="Pdf"/>
                                                <span className="content-docs__text">Hikoot_Proposal</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Psd} className="content-docs__img" alt="Psd"/>
                                                <span className="content-docs__text">Hikoot_Desktop</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pptx} className="content-docs__img" alt="Pptx"/>
                                                <span className="content-docs__text">Hikoot_Presentation</span>
                                            </Link>
                                        </div>
                                    </TabPane>
                                    <TabPane tabId="2" className="files-item_tab-content">
                                        <div className="files-item_content">
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Psd} className="content-docs__img" alt="Psd"/>
                                                <span className="content-docs__text">Hikoot_Desktop</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pptx} className="content-docs__img" alt="Pptx"/>
                                                <span className="content-docs__text">Hikoot_Presentation</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Doc} className="content-docs__img" alt="Doc"/>
                                                <span className="content-docs__text">Hikoot_Concept</span>
                                            </Link>
                                            <Link to={"/"} className="files-item_content-docs">
                                                <img src={Pdf} className="content-docs__img" alt="Pdf"/>
                                                <span className="content-docs__text">Hikoot_Proposal</span>
                                            </Link>
                                        </div>
                                    </TabPane>
                                </TabContent>
                            </TabPane>
                        </TabContent>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => bindActionCreators({
},dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Documents);

