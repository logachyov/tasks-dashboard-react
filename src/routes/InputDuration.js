import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Select from 'react-select';
import { Link } from 'react-router-dom';
import { AreaChart, Area, Tooltip, ResponsiveContainer} from 'recharts';


class InputDuration extends React.Component {

const options = [
{value: 'twoHour', label: '2 hours before event'},
    {value: 'fourHour', label: '4 hours before event'},
    {value: 'sixHour', label: '6 hours before event'},
    {value: 'eightHour', label: '8 hours before event'},
    {value: 'tenHour', label: '10 hours before event'},
    {value: 'twelveHour', label: '12 hours before event'},
    {value: 'oneDay', label: '1 day before event'},
    {value: 'twoDay', label: '2 days before event'},
    {value: 'threeDay', label: '3 days before event'},
    {value: 'oneWeek', label: '1 week before event'},
];

    render() {
        const { selectedOption } = this.state;
        /*Custom styles for select*/
        const customStylesSelect = {
            indicatorSeparator: () => ({
                alignSelf: 'stretch',
                backgroundColor: "#E9EFF4",
                marginBottom: 0,
                marginTop: 0,
                width: 1,
                boxSizing: 'border-box',
            }),
            indicatorsContainer: () => ({
                display: 'flex',
                alignItems: 'center',
                alignSelf: 'stretch',
                flexShrink: 0,
                boxSizing: 'border-box',
                backgroundColor: 'rgba(244, 247, 249, 0.4)',
            }),
            indicatorContainer: () => ({
                display: 'flex',
                boxSizing: 'border-box',
                transition: 'color 150ms',
                color: '#808FA3;',
                padding: 12,
            }),
            control: () => ({
                display: 'flex',
                alignItems: 'center',
                boxSizing: 'border-box',
                backgroundColor: '#FFFFFF',
                borderColor: '#E9EFF4',
                borderRadius: 4,
                borderStyle: 'solid',
                borderWidth: 1,
                cursor: 'default',
                flexWrap: 'wrap',
                justifyContent: 'space-between',
                minHeight: 45,
                outline: '0 !important',
                position: 'relative',
                transition: 'all 100ms',
            }),
            dropdownIndicator: () => ({
                display: 'flex',
                padding: 12.5,
                color: '#808FA3',
                transition: 'color 150ms',
                boxSizing: 'border-box',
            }),
            valueContainer: () => ({
                display: 'flex',
                alignItems: 'center',
                flex: 1,
                flexWrap: 'wrap',
                padding: '6px 8px 5px 20px',
                position: 'relative',
                overflow: 'hidden',
                boxSizing: 'border-box',
            }),
            placeholder: () => ({
                color: '#323C47',
                opacity: 0.3,
                position: 'absolute',
                top: '50%',
                transform: 'translateY(-50%)',
                boxSizing: 'border-box',
            }),
            singleValue: () => ({
                color: '#323C47',
                maxWidth: 'calc(100% - 8px)',
                overflow: 'hidden',
                position: 'absolute',
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
                top: '50%',
                transform: 'translateY(-50%)',
                boxSizing: 'border-box',
                opacity: 1,
                transition: 'opacity 300ms',
            }),
            menuList: () => ({
                maxHeight: 170,
                overflowY: 'auto',
                position: 'relative',
                boxSizing: 'border-box',
            }),
            option: (provided, state) => ({
                backgroundColor: state.isFocused ? '#18DBFF' : '#FFFFFF',
                cursor: 'default',
                display: 'block',
                padding: '8px 20px',
                width: '100%',
                userSelect: 'none',
                boxSizing: 'border-box',
            }),
        }

        return (
            <div>
                <Select className="select-item"
                        value={selectedOption}
                        onChange={this.handleChange}
                        options={options}
                        placeholder={"Select reminder time"}
                        styles={customStylesSelect}
                />
            </div>
        )
    }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => bindActionCreators({
},dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(InputDuration);

