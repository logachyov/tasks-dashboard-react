import React from 'react'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ObjectCard from '../components/ObjectCard.js'
import AddIcon from '../images/add-icon.svg';
import PrintIcon from '../images/printer-icon.svg';
import TrashIcon from '../images/trash-icon.svg';
import {getObjects} from '../store/objects'


 class AugmentedExperiencesList extends React.Component {
     constructor(props){
         super(props);
         this.state = {

         }

     }

     componentWillMount() {
         this.props.getObjects()
     }

     render() {
         let {objects} = this.props;
         return(
             <div>
                 <div className="search-section">
                     <form className="form">
                         <div className="row">
                             <div className="form-group col-md-3">
                                 <input type="search" className="form-control input-primary" id="searchInput"
                                        placeholder="Search" name=""/>
                             </div>
                             <div className="form-group col-md-3">
                                 <select className="input-primary select-primary" defaultValue="Sort by">
                                     <option value="1">By date</option>
                                     <option value="2">By type</option>
                                     <option value="3">By alphabet</option>
                                 </select>
                             </div>
                             <div className="form-group col-md-3 custom-control custom-checkbox">
                                 <input type="checkbox" className="custom-control-input" id="customCheck1"/>
                                 <label className="custom-control-label" htmlFor="customCheck1">Show only without assets</label>
                             </div>
                         </div>
                         <button hidden type="submit" className="btn btn-primary">Submit</button>
                     </form>
                 </div>
                 <div className="object-list">
                     <div className="row">
                         {objects.map((object_item, i) => {
                            return <ObjectCard object_item={object_item} key={i}/>
                         })}
                     </div>


                 </div>
                 <div className="action-section">
                     <button className="btn btn-action">
                         <img src={AddIcon} alt=""/>
                     </button>
                     <button className="btn btn-action">
                         <img src={PrintIcon} alt=""/>
                     </button>
                     <button className="btn btn-action btn-action_delete">
                         <img src={TrashIcon} alt=""/>
                     </button>
                 </div>
             </div>

         )
     }
 }

const mapStateToProps = state => ({
    objects: state.objects.objects
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getObjects
},dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AugmentedExperiencesList);

