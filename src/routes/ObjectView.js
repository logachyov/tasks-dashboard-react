import React from 'react';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ObjectImage from '../images/object-image1.png';
import ArrowIcon from '../images/next arrow.svg';
import AddIcon from '../images/add-icon.svg';
import TrashIcon from '../images/trash-icon.svg';
import SaveIcon from '../images/save-icon.svg';
import qrCode from '../images/qr-code.png';
import bindIcon from '../images/bind-icon.svg';
import separatorIcon from '../images/separator-icon.svg';
import AssetCard from '../components/AssetCard';
import TagsInput from 'react-tagsinput'

class ObjectView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tags: props.location.object_item.hashtags
        }
    }

    handleChange(tags) {
        this.setState({tags})
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    renderTag (props) {
        let {tag, key, disabled, onRemove, classNameRemove, getTagDisplayValue, ...other} = props
        return (
            <span key={key} {...other}>
                <span className="tag-item__value">{getTagDisplayValue(tag)}</span>
                {!disabled &&
                    <a className={classNameRemove} onClick={(e) => onRemove(key)} />
                }
            </span>
        )
    }

    renderLayout(tagComponents, inputComponent) {
        return (
            <span className="input-tag-inner">
                {inputComponent}
                <span className="tag__container">
                   {tagComponents}
                </span>
            </span>
        )
    }

    render() {
        const {object_item} = this.props.location;
        return (
            <div>
                <div className="object-view">
                    <div className="subheader">
                        <div className="row">
                            <div className="col-md-4">
                                <button className="btn btn-back">
                                    <img src={ArrowIcon} className="subheader__arrow" alt=""/>
                                </button>
                                <span className="subheader__title">Edit object</span>
                            </div>
                            <div className="col-md-5">
                                <div className="row">
                                    <div className="col-md-6">
                                        <label className="custom-control switch justify-content-between">Select status:
                                            <input type="checkbox" className="switch-control-input"/>
                                            <span className="switch-control-indicator">
                                            <span className="switch-control-description switch-control-description_off">Inactive</span>
                                            <span className="switch-control-description switch-control-description_on">Active</span>
                                        </span>
                                        </label>
                                    </div>
                                    <div className="col-md-6 text-right">
                                        <Link to="training-images" className="btn btn-gradient">Add training images</Link>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3 text-right">
                                <button className="btn btn-with-icon btn-gradient">
                                    <img className="btn__icon" src={SaveIcon} alt=""/>
                                    Save object</button>
                            </div>
                        </div>
                    </div>
                    <div className="object-details">
                        <div className="row">
                            <div className="col-md-4">
                                <img src={ObjectImage} alt="" className="object-view__image"/>
                            </div>
                            <div className="col-md-5 d-flex flex-column justify-content-between">
                                <div className="form-group">
                                    <input
                                        type="text" className="form-control input-primary d-block" id="searchInput"
                                        placeholder="Object name" name="object-name" value={object_item.name}
                                        onChange={this.handleInputChange.bind(this)}/>
                                </div>
                                <div className="form-group">
                                    <input
                                        type="text" className="form-control input-primary d-block" id="searchInput"
                                        placeholder="Object description" name="object-description"/>
                                </div>
                                <div className="form-group">
                                    <input
                                        type="text" className="form-control input-primary d-block" id="searchInput"
                                        placeholder="Object location" name="object-location"/>
                                </div>
                                <div className="form-group">
                                    <TagsInput
                                        renderLayout={this.renderLayout.bind(this)}
                                        renderTag={this.renderTag.bind(this)}
                                        className="input-tag-wrapper" focusedClassName="input-tag-wrapper_focused"
                                        value={this.state.tags} onChange={this.handleChange.bind(this)}
                                        inputProps={{className: 'input-primary input-tag',placeholder: 'Add new tag'}} onlyUnique
                                        tagProps={{className: 'tag-item', classNameRemove: 'tag-item__remove'}}/>
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="object-details__qr">
                                    <img src={qrCode} alt="" className="object-details__qr-image"/>
                                    <span className="object-details__qr-text">Tap to print the QR code</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="assets-section">
                        <div className="subheader">
                            <div className="row">
                                <div className="col-md-3">
                                    <div className="subheader__title assets-list__title">Bind assets to the object</div>
                                </div>
                                <div className="col-md-9 text-center">
                                    <button className="btn assets-list__separator">
                                        <img className="assets-list__separator-image" src={separatorIcon} alt=""/>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="search-section">
                            <form className="form">
                                <div className="row">
                                    <div className="form-group col-md-3">
                                        <input type="search" className="form-control input-primary" id="searchInput"
                                               placeholder="Search" name=""/>
                                    </div>
                                    <div className="form-group col-md-3">
                                        <select className="input-primary select-primary" defaultValue="Select type of asset">
                                            <option value="1">By date</option>
                                            <option value="2">By type</option>
                                            <option value="3">By alphabet</option>
                                        </select>
                                    </div>
                                    <div className="form-group col-md-3">
                                        <select className="input-primary select-primary" defaultValue="Sort by">
                                            <option value="1">By date</option>
                                            <option value="2">By type</option>
                                            <option value="3">By alphabet</option>
                                        </select>
                                    </div>
                                    <div className="form-group col-md-3 custom-control custom-checkbox">
                                        <input type="checkbox" className="custom-control-input" id="customCheck1"/>
                                        <label className="custom-control-label" htmlFor="customCheck1">Show only without assets</label>
                                    </div>
                                </div>
                                <button hidden type="submit" className="btn btn-primary">Submit</button>
                            </form>
                        </div>
                        <div className="assets-list">
                            <div className="row">
                                <div className="col-md-2">
                                    <AssetCard />
                                </div>
                                <div className="col-md-2">
                                    <AssetCard />
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div className="action-section">
                    <button className="btn btn-action">
                        <img src={AddIcon} alt=""/>
                    </button>
                    <button className="btn btn-action">
                        <img src={bindIcon} alt=""/>
                    </button>
                    <button className="btn btn-action btn-action_delete">
                        <img src={TrashIcon} alt=""/>
                    </button>
                </div>
            </div>

        )
    }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => bindActionCreators({
},dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ObjectView);

