import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ReactSVG from 'react-svg'
import { Link } from 'react-router-dom';
import { AreaChart, Area, Tooltip, ResponsiveContainer} from 'recharts';
import paperclip from '../images/paperclip.svg'

class Meetings extends React.Component {

    render() {
        return (
            <div>
                <div className="row header-title">
                    <div className="col-md-6">
                        <div className="meetings-header">
                            <h1 className="all-title meetings-title">Meetings</h1>
                            <Link className="btn meetings-header__export-btn" to={"/meetings/createevent"} role="button">
                                <span className="meetings-header__export-text">+ Create new</span>
                            </Link>
                        </div>
                    </div>
                    <div className="col-md-6 d-flex flex-row justify-content-end">
                        <div className="btn-group btn-group-toggle meetings-header__view" data-toggle="buttons">
                            <label className="btn meetings-header__view-cards active">
                                <input type="radio" name="view" id="viewDay" autoComplete="off" checked/>
                                <span className="meetings-header__view-text">Day</span>
                            </label>
                            <label className="btn meetings-header__view-cards">
                                <input type="radio" name="view" id="viewWeek" autoComplete="off"/>
                                <span className="meetings-header__view-text">Week</span>
                            </label>
                            <label className="btn meetings-header__view-cards">
                                <input type="radio" name="view" id="viewMonth" autoComplete="off"/>
                                <span className="meetings-header__view-text">Month</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div className="row flex-nowrap overflow-auto meetings-scroll">
                    <div className="col-md-4">
                        <div className="meetings-card colour-white">
                            <h3 className="meetings-card__title meetings-card__title-active">
                                <span className="meetings-card__title-number">02</span><span className="meetings-card__title-ordinal">nd</span> Thursday
                            </h3>
                            <div className="task-list">
                                <span className="task-list__time">11:00 AM - 11:30 AM</span>
                                <span className="task-list__text">Inspiration gathering</span>
                                <div className="row d-flex align-items-center">
                                    <div className="col-md-6 d-flex">
                                        <button type="button" className="btn invite-btn">
                                            <i className="far fa-paper-plane invite-btn__icon"/>
                                        </button>
                                        <Link className="invite-avatar" to={"/"}>
                                            <span className="avatar-icon avatar-icon__one">AM</span>
                                        </Link>
                                        <Link className="invite-avatar" to={"/"}>
                                            <span className="avatar-icon avatar-icon__two">ES</span>
                                        </Link>
                                    </div>
                                    <div className="col-md-6 d-flex justify-content-end">
                                        <Link className="invite-attachement" to={"/"}>
                                            <i className="fas fa-paperclip invite-attachement__icon"/>10 attachements
                                        </Link>
                                    </div>
                                </div>
                            </div>
                            <div className="task-list task-list-active">
                                <span className="task-list__time task-list__time-active">11:00 AM - 11:30 AM</span>
                                <span className="task-list__text task-list__text-active">Sketching</span>
                                <div className="row">
                                    <div className="col-md-6 d-flex">
                                        <button type="button" className="btn invite-btn invite-btn__active">
                                            <i className="far fa-paper-plane invite-btn__icon invite-btn__icon-active"/>+
                                        </button>
                                        <Link className="invite-avatar invite-avatar__active" to={"/"}>
                                            <span className="avatar-icon avatar-icon__one avatar-icon__active">AM</span>
                                        </Link>
                                        <Link className="invite-avatar invite-avatar__active" to={"/"}>
                                            <span className="avatar-icon avatar-icon__two avatar-icon__active">BK</span>
                                        </Link>
                                    </div>
                                    <div className="col-md-6 d-flex justify-content-end">
                                        <Link className="invite-attachement invite-attachement__active" to={"/"}>
                                            <i className="fas fa-paperclip invite-attachement__icon invite-attachement__icon-active"/>3 attachements
                                        </Link>
                                    </div>

                                </div>
                            </div>
                            <div className="task-list">
                                <span className="task-list__time">11:00 AM - 11:30 AM</span>
                                <span className="task-list__text">Hikoot app Wireframes</span>
                                <div className="row">
                                    <div className="col-md-6 d-flex">
                                        <button type="button" className="btn invite-btn">
                                            <i className="far fa-paper-plane invite-btn__icon"/>
                                        </button>
                                        <Link className="invite-avatar" to={"/"}>
                                            <span className="avatar-icon avatar-icon__three">LA</span>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="meetings-card colour-white">
                            <h3 className="meetings-card__title">
                                <span className="meetings-card__title-number">03</span><span className="meetings-card__title-ordinal">rd</span> Friday
                            </h3>
                            <div className="task-list">
                                <span className="task-list__time">11:00 AM - 11:30 AM</span>
                                <span className="task-list__text">Website for the Product</span>
                                <div className="row">
                                    <div className="col-md-6 d-flex">
                                        <button type="button" className="btn invite-btn">
                                            <i className="far fa-paper-plane invite-btn__icon"/>
                                        </button>
                                        <Link className="invite-avatar" to={"/"}>
                                            <span className="avatar-icon avatar-icon__one">AM</span>
                                        </Link>
                                        <Link className="invite-avatar" to={"/"}>
                                            <span className="avatar-icon avatar-icon__two">ES</span>
                                        </Link>
                                    </div>
                                    <div className="col-md-6 d-flex justify-content-end">
                                        <Link className="invite-attachement" to={"/"}>
                                            <i className="fas fa-paperclip invite-attachement__icon"/>10 attachements
                                        </Link>
                                    </div>
                                </div>
                            </div>
                            <div className="task-list">
                                <span className="task-list__time">11:00 AM - 11:30 AM</span>
                                <span className="task-list__text">Hey you guys we need to do this and that too ✌</span>
                                <div className="row">
                                    <div className="col-md-6 d-flex">
                                        <button type="button" className="btn invite-btn">
                                            <i className="far fa-paper-plane invite-btn__icon"/>
                                        </button>
                                        <Link className="invite-avatar" to={"/"}>
                                            <span className="avatar-icon avatar-icon__three">LA</span>
                                        </Link>
                                        <Link className="invite-avatar" to={"/"}>
                                            <span className="avatar-icon avatar-icon__one">AM</span>
                                        </Link>
                                        <Link className="invite-avatar" to={"/"}>
                                            <span className="avatar-icon avatar-icon__four">HS</span>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="meetings-card colour-white">
                            <h3 className="meetings-card__title">
                                <span className="meetings-card__title-number">06</span><span className="meetings-card__title-ordinal">th</span> Monday
                            </h3>
                            <div className="task-list">
                                <span className="task-list__time">09:50 AM - 13:25 AM</span>
                                <span className="task-list__text">Start usability testing</span>
                                <div className="row">
                                    <div className="col-md-6 d-flex">
                                        <button type="button" className="btn invite-btn">
                                            <i className="far fa-paper-plane invite-btn__icon"/>
                                        </button>
                                        <Link className="invite-avatar" to={"/"}>
                                            <span className="avatar-icon avatar-icon__one">TX</span>
                                        </Link>
                                        <Link className="invite-avatar" to={"/"}>
                                            <span className="avatar-icon avatar-icon__two">ES</span>
                                        </Link>
                                    </div>
                                    <div className="col-md-6 d-flex justify-content-end">
                                        <Link className="invite-attachement" to={"/"}>
                                            <i className="fas fa-paperclip invite-attachement__icon"/>10 attachements
                                        </Link>
                                    </div>
                                </div>
                            </div>
                            <div className="task-list">
                                <span className="task-list__time">14:00 AM - 16:30 AM</span>
                                <span className="task-list__text">Open discussion</span>
                                <div className="row">
                                    <div className="col-md-6 d-flex">
                                        <button type="button" className="btn invite-btn">
                                            <i className="far fa-paper-plane invite-btn__icon"/>
                                        </button>
                                        <Link className="invite-avatar" to={"/"}>
                                            <span className="avatar-icon avatar-icon__three">LA</span>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                            <div className="task-list task-list__none">
                                <span className="task-list__time">16:35 AM - 17:00 AM</span>
                                <span className="task-list__text task-list__text-none">Daily report</span>
                                <Link className="invite-attachement invite-attachement__none" to={"/"}>
                                    <i className="fas fa-paperclip invite-attachement__icon"/>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="meetings-card colour-white">
                            <h3 className="meetings-card__title meetings-card__title-active">
                                <span className="meetings-card__title-number">08</span><span className="meetings-card__title-ordinal">nd</span> Thursday
                            </h3>
                            <div className="task-list">
                                <span className="task-list__time">11:00 AM - 11:30 AM</span>
                                <span className="task-list__text">Inspiration gathering</span>
                                <div className="row">
                                    <div className="col-md-6 d-flex">
                                        <button type="button" className="btn invite-btn">
                                            <i className="far fa-paper-plane invite-btn__icon"/>
                                        </button>
                                        <Link className="invite-avatar" to={"/"}>
                                            <span className="avatar-icon avatar-icon__one">AM</span>
                                        </Link>
                                        <Link className="invite-avatar" to={"/"}>
                                            <span className="avatar-icon avatar-icon__two">ES</span>
                                        </Link>
                                    </div>
                                    <div className="col-md-6 d-flex justify-content-end">
                                        <Link className="invite-attachement" to={"/"}>
                                            <i className="fas fa-paperclip invite-attachement__icon"/>10 attachements
                                        </Link>
                                    </div>
                                </div>
                            </div>
                            <div className="task-list">
                                <span className="task-list__time">11:00 AM - 11:30 AM</span>
                                <span className="task-list__text">Hikoot app Wireframes</span>
                                <div className="row">
                                    <div className="col-md-6 d-flex">
                                        <button type="button" className="btn invite-btn">
                                            <i className="far fa-paper-plane invite-btn__icon"/>
                                        </button>
                                        <Link className="invite-avatar" to={"/"}>
                                            <span className="avatar-icon avatar-icon__three">LA</span>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => bindActionCreators({
},dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Meetings);

