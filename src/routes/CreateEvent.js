import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import 'rc-input-number/assets/index.css';
import '../styles/components/inputHHmm.css'
import InputHour from './InputHour';
import InputMinute from './InputMinute';
import { AreaChart, Area, Tooltip, ResponsiveContainer} from 'recharts';
import { Button, Form, CustomInput, FormGroup, Label, Input, FormText } from 'reactstrap';
import Select from 'react-select';
import CalendarSide from "../components/CalendarSide";


const options = [
    { value: 'twoHour', label: '2 hours before event' },
    { value: 'fourHour', label: '4 hours before event' },
    { value: 'sixHour', label: '6 hours before event' },
    { value: 'eightHour', label: '8 hours before event' },
    { value: 'tenHour', label: '10 hours before event' },
    { value: 'twelveHour', label: '12 hours before event' },
    { value: 'oneDay', label: '1 day before event' },
    { value: 'twoDay', label: '2 days before event' },
    { value: 'threeDay', label: '3 days before event' },
    { value: 'fourDay', label: '4 days before event' },
    { value: 'oneWeek', label: '1 week before event' },
]

class CreateEvent extends React.Component {

    state = {
        selectedOption: null,
    };


    handleChange = selectedOption => {
        this.setState(
            { selectedOption },
            () => console.log(`Option selected:`, this.state.selectedOption)
        );
    };


    render() {
        const { selectedOption } = this.state;
        /*Custom styles for select*/
        const customStylesSelect = {
            indicatorSeparator: () => ({
                alignSelf: 'stretch',
                backgroundColor: "#E9EFF4",
                marginBottom: 0,
                marginTop: 0,
                width: 1,
                boxSizing: 'border-box',
            }),
            indicatorsContainer: () => ({
                display: 'flex',
                alignItems: 'center',
                alignSelf: 'stretch',
                flexShrink: 0,
                boxSizing: 'border-box',
                backgroundColor: 'rgba(244, 247, 249, 0.4)',
            }),
            indicatorContainer: () => ({
                display: 'flex',
                boxSizing: 'border-box',
                transition: 'color 150ms',
                color: '#808FA3;',
                padding: 12,
            }),
            control: () => ({
                display: 'flex',
                alignItems: 'center',
                boxSizing: 'border-box',
                backgroundColor: '#FFFFFF',
                borderColor: '#E9EFF4',
                borderRadius: 4,
                borderStyle: 'solid',
                borderWidth: 1,
                cursor: 'default',
                flexWrap: 'wrap',
                justifyContent: 'space-between',
                minHeight: 45,
                outline: '0 !important',
                position: 'relative',
                transition: 'all 100ms',
            }),
            dropdownIndicator: () => ({
                display: 'flex',
                padding: 12.5,
                color: '#808FA3',
                transition: 'color 150ms',
                boxSizing: 'border-box',
            }),
            valueContainer: () => ({
                display: 'flex',
                alignItems: 'center',
                flex: 1,
                flexWrap: 'wrap',
                padding: '6px 8px 5px 20px',
                position: 'relative',
                overflow: 'hidden',
                boxSizing: 'border-box',
            }),
            placeholder: () => ({
                color: '#323C47',
                opacity: 0.3,
                position: 'absolute',
                top: '50%',
                transform: 'translateY(-50%)',
                boxSizing: 'border-box',
            }),
            singleValue: () => ({
                color: '#323C47',
                maxWidth: 'calc(100% - 8px)',
                overflow: 'hidden',
                position: 'absolute',
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
                top: '50%',
                transform: 'translateY(-50%)',
                boxSizing: 'border-box',
                opacity: 1,
                transition: 'opacity 300ms',
            }),
            menuList: () => ({
                maxHeight: 170,
                overflowY: 'auto',
                position: 'relative',
                boxSizing: 'border-box',
            }),
            option: (provided, state) => ({
                backgroundColor: state.isFocused ? '#18DBFF' : '#FFFFFF',
                cursor: 'default',
                display: 'block',
                padding: '8px 20px',
                width: '100%',
                userSelect: 'none',
                boxSizing: 'border-box',
            }),
        }

        return (
            <div>
                <div className="row d-flex align-items-center header-title">
                    <div className="col-md-6">
                        <h1 className="all-title">Create event</h1>
                    </div>
                    <div className="col-md-6 d-flex justify-content-end">
                        <Link className="btn create-cancel d-flex" to={"/meetings"} role="button">
                            <span className="create-cancel__text">Cancel</span>
                        </Link>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-8">
                        <div className="event-item colour-white">
                            <div className="event-item__title">
                                <span className="event-item__title-text">Title</span>
                                <div className="input-group event-item__input">
                                    <input type="text" className="form-control input-form" placeholder="Enter a description" aria-label="Enter a description" aria-describedby="add-description"/>
                                    <div className="input-group-append input-add">
                                        <button className="btn input-add__btn" type="button" id="add-description">+ Add description</button>
                                    </div>
                                </div>
                            </div>
                            <div className="event-item__date">
                                <div className="event-item__date-day">
                                    <span className="event-date__title">Day</span>
                                    <CalendarSide
                                        showInput
                                    />
                                </div>
                                <div className="event-item__date-hour">
                                    <span className="event-date__title">Hour</span>
                                    <InputHour/>
                                </div>
                                <div className="event-item__date-minute">
                                    <span className="event-date__title">Minute</span>
                                    <InputMinute/>
                                </div>
                                <div className="event-item__date-duration">
                                    <span className="event-date__title">Duration</span>

                                </div>
                            </div>
                            <div className="event-item__location">
                                <span className="event-item__location-text">Location</span>
                                <div className="input-group event-item__input">
                                    <input type="text" className="form-control input-form input-form__location" placeholder="Enter location" aria-label="Enter location" aria-describedby="add-location"/>
                                    <div className="input-group-append input-add">
                                        <button className="btn input-add__btn" type="button" id="add-location">+ Set meeting room</button>
                                    </div>
                                </div>
                            </div>
                            <div className="event-item__upload">
                                <span className="event-item__upload-text">Upload attachements</span>
                                <div className="event-upload__btn-files">
                                    <div className="event-files">
                                        <span className="event-document__icon"><i className="far fa-file event-document__icon-style"/></span>
                                        <span className="event-document event-document__name">Hikoot_Clien_Pitch_V2.pdf</span>
                                        <span className="event-document event-document__note">+ Add note</span>
                                        <span className="event-document">Edit</span>
                                        <span className="event-document">Remove</span>
                                    </div>
                                    <button className="btn event-btn" type="submit">+ Add files</button>
                                </div>
                                <div className="event-upload__drop-files">
                                    <span className="drop-files__title"><i className="fas fa-cloud-upload-alt drop-files__title-icon"/>You can also drop your files here
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="members-item colour-white ">
                            {/*Add team members*/}
                            <div className="members-item__team">
                                <span className="members-item__title">Add team mebers</span>
                                <div className="members-item__team-invite">
                                    <span className="avatar-icon avatar-icon__three members-item__team-avatar">LA
                                        <i className="fas fa-check avatar-icon__check avatar-icon__check-three"/>
                                    </span>
                                    <span className="avatar-icon avatar-icon__one members-item__team-avatar">AM
                                        <i className="fas fa-check avatar-icon__check avatar-icon__check-one"/>
                                    </span>
                                    <span className="avatar-icon avatar-icon__two members-item__team-avatar">ES
                                        <i className="fas fa-check avatar-icon__check avatar-icon__check-two"/>
                                    </span>
                                    <button type="button" className="btn members-item__team-btn">
                                        <i className="far fa-paper-plane members-item__team-icon"/>+
                                    </button>
                                </div>
                            </div>
                            {/*Add guests*/}
                            <div className="members-item__guests">
                                <span className="members-item__title">Add guests</span>
                                <div className="input-group event-item__input">
                                    <input type="text" className="form-control input-form" placeholder="Email invitation" aria-label="Email invitation" aria-describedby="add-Email"/>
                                    <div className="input-group-append input-add">
                                        <button className="btn input-add__btn" type="button" id="add-Email">Send</button>
                                    </div>
                                </div>
                            </div>
                            {/*Notify people on*/}
                            <FormGroup className="members-item__notify">
                                <Label className="members-item__title" for="notifyCheckbox">Notify people on</Label>
                                <div className="members-item__notify-checkbox">
                                    <CustomInput className="members-notify__checkbox" type="checkbox" id="notifyCustomCheckbox" label="Slack" />
                                    <CustomInput className="members-notify__checkbox" type="checkbox" id="notifyCustomCheckbox2" label="HipChat" />
                                </div>
                            </FormGroup>
                            {/*Set reminder*/}
                            <span className="members-item__title">Set reminder</span>
                            <Select className="select-item"
                                value={selectedOption}
                                onChange={this.handleChange}
                                options={options}
                                placeholder={"Select reminder time"}
                                styles={customStylesSelect}
                            />
                            {/*Button "Create event"*/}
                            <button type="button" className="btn members-item__create">Create event</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => bindActionCreators({
},dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CreateEvent);

