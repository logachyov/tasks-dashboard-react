import React, { PropTypes } from 'react'
import { Link, NavLink } from 'react-router-dom';
import { Progress } from 'reactstrap';
import { CustomInput, Form, FormGroup, Label } from 'reactstrap';
import cloudUploader from '../images/cloud-uploader.svg'
import Sketches1 from "../images/sketches1.png";
import Sketches2 from "../images/sketches2.png";
import ReactSVG from "react-svg";

class SidebarUploader extends React.Component {

    render(){

        return(
            <div>
                <div className="sidebar-uploader">
                    <div className="media sidebar-uploader__header">
                        <button type="button" className="btn sidebar-uploader__header-btn">
                            <ReactSVG className="uploader-header__icon" src={cloudUploader} alt=""/>
                        </button>
                        <div className="media-body sidebar-uploader__header-text">
                            <h5 className="uploader-header__title">Uploader</h5>
                            <span className="uploader-header__subtitle">14 March</span>
                        </div>
                    </div>
                    <div className="sidebar-uploader__content">
                        <div className="download-progressbar">
                            <div className="download-progressbar__style">
                                <Progress className="download-progressbar__style-progress download-progressbar__successful" value="10.9" max="10.9" />
                                <span className="download-successful">
                                    <i className="fas fa-check download-successful__icon"/>
                                </span>
                            </div>
                            <div className="download-progressbar__subtitle text-left">10,9
                                <span className="download-progressbar__subtitle-index">MB</span>
                            </div>
                            <div className="download-progressbar__file">
                                <span className="download-progressbar__file-name">User_interviews.pdf</span>
                            </div>
                            <Link className="download-progressbar__view" to={"/"}>View file</Link>
                        </div>
                        <div className="download-progressbar">
                            <div className="download-progressbar__style">
                                <Progress className="download-progressbar__style-progress download-progressbar__progress" value="2.44" max="7.48" />
                                <button className="btn download-cancel" type="submit">
                                    <i className="fas fa-times download-cancel__icon"/>
                                </button>
                            </div>
                            <div className="download-progressbar__subtitle text-left">7,48
                                <span className="download-progressbar__subtitle-index">MB</span>
                            </div>
                            <div className="download-progressbar__file">
                                <img className="download-progressbar__file-img" src={Sketches1} alt=""/>
                                <span className="download-progressbar__file-name">Sketches_01.jpeg</span>
                            </div>
                            <Link className="download-progressbar__view download-progressbar__view-no" to={"/"}>View file</Link>
                        </div>
                        <div className="download-progressbar">
                            <div className="download-progressbar__style">
                                <Progress className=" download-progressbar__style-progress" value="0" max="6.0"/>
                                <button className="btn download-cancel" type="submit">
                                    <i className="fas fa-times download-cancel__icon"/>
                                </button>
                            </div>
                            <div className="download-progressbar__subtitle text-left">6,00
                                <span className="download-progressbar__subtitle-index">MB</span>
                            </div>
                            <div className="download-progressbar__file">
                                <img className="download-progressbar__file-img" src={Sketches2} alt=""/>
                                <span className="download-progressbar__file-name">Sketches_02.jpeg</span>
                            </div>
                            <Link className="download-progressbar__view download-progressbar__view-no" to={"/"}>View file</Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

SidebarUploader.propTypes = {
    // object_item: PropTypes.object.isRequired,
};
export default SidebarUploader;

