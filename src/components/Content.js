import React from 'react';
import { Route } from 'react-router-dom';

import Home from "../routes/AugmentedExperiencesList";
import AssetsList from "../routes/AssetsList";
 const Content = () => (
     <div>
         <Route exact path="/" component={Home} />
         <Route exact path="/assets-list" component={AssetsList} />
     </div>
 );

export default Content;