import React, { PropTypes } from 'react'
import { Link, NavLink } from 'react-router-dom';
import logotype from '../images/menu/logo.png'
import dashboard from '../images/menu/dashboard-icon.svg'
import session from '../images/menu/sessions-icon.svg'
import meeting from '../images/menu/meetings-icon.svg'
import files from '../images/menu/filesboard-icon.svg'
import conversation from '../images/menu/conversations-icon.svg'
import setting from '../images/menu/settings-icon.svg'

class Menu extends React.Component {

    render(){

        return(
            <div>
                <nav className="navbar navbar-expand-lg menu-item">
                    <Link to={"/"} className="navbar-brand menu-item__header">
                        <img src={logotype} className="menu-item__header-logo" alt="Home"/>
                    </Link>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navMenu"
                            aria-controls="navMenu" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"/>
                    </button>
                    <div className="collapse navbar-collapse menu-wrap" id="navMenu">
                        <ul className="navbar-nav menu-item__nav">
                            <li className="nav-item menu-item__nav-point">
                                <NavLink exact to={"/"} className="nav-link nav-point__link" activeClassName="active">
                                    <img src={dashboard} className="nav-point__link-icon" alt="Home"/>
                                </NavLink>
                            </li>
                            <li className="nav-item menu-item__nav-point">
                                <NavLink exact to={"/sessions"} className="nav-link nav-point__link" activeClassName="active">
                                    <img src={session} className="nav-point__link-icon" alt="Sessions"/>
                                </NavLink>
                            </li>
                            <li className="nav-item menu-item__nav-point">
                                <NavLink exact to={"/meetings"} className="nav-link nav-point__link" activeClassName="active">
                                    <img src={meeting} className="nav-point__link-icon" alt="Meetings"/>
                                </NavLink>
                            </li>
                            <li className="nav-item menu-item__nav-point">
                                <NavLink exact to={"/documents"} className="nav-link nav-point__link" activeClassName="active">
                                    <img src={files} className="nav-point__link-icon" alt="Files"/>
                                </NavLink>
                            </li>
                            <li className="nav-item menu-item__nav-point">
                                <NavLink exact to={"/5"} className="nav-link nav-point__link" activeClassName="active">
                                    <img src={conversation} className="nav-point__link-icon" alt="Conversations"/>
                                </NavLink>
                            </li>
                            <li className="nav-item menu-item__nav-point">
                                <NavLink exact to={"/6"} className="nav-link nav-point__link" activeClassName="active">
                                    <img src={setting} className="nav-point__link-icon" alt="Settings"/>
                                </NavLink>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        )
    }

}

Menu.propTypes = {
    // object_item: PropTypes.object.isRequired,
};
export default Menu;

