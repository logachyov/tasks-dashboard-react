import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import logo from '../images/logo.svg';
import userLogo from '../images/user_icon.svg';



const Header = () => (
    <header className="header">
        <nav className="navbar navbar-expand-lg navbar-dark">
            <Link to="/" className="navbar-brand">
                <img src={logo} alt="3rdi logo" className="logo"/>
                <span className="logo__text">ELIXR</span>
            </Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"/>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav ml-auto">
                    <li className="nav-item">
                        <NavLink exact to="/statistics" className="nav-link" >Dtatistics</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink exact to="/" className="nav-link" >Augmented Experiences</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink exact to="/assets-list" className="nav-link" activeClassName="active">Assets</NavLink>
                    </li>
                    <li className="nav-item dropdown">
                        <button className="nav-link dropdown-toggle navbar-user-dropdown" id="navbarDropdown"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img className="navbar-user-dropdown__avatar" src={userLogo} alt="user avatar"/>
                            <span className="navbar-user-dropdown__name">Raphael Walker</span>
                        </button>
                        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                            <span className="dropdown-item">Settings</span>
                            <span className="dropdown-item">Logout</span>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
);


 export default Header;