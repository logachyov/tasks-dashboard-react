import React, { PropTypes } from 'react'
import { Link } from 'react-router-dom';
import ObjectImage from '../images/object-image1.png';

class ObjectCard extends React.Component {
    constructor(props){
        super(props);
        this.state = {

        }

    }

    renderHashTag(tag_item,i,length){
        if(i <= 2){
            return(
                <div className="object-item__tags-item" key={i}>#{tag_item}</div>
            )
        }
    }

    renderHashTagMore(length){
        if(length > 2){
            return <div className="object-item__tags-item object-item__tags-item_underline">+{length -2} more</div>
        }
    }

    render(){
        const {object_item} = this.props;

        return(
            <div className="col-md-3 col-sm-6 col-12">
                <Link className="object-item" to={{ pathname: '/object-view', object_item }}>
                    <img src={ObjectImage} alt="" className="object-item__image"/>
                    <div className="object-item__body">
                        <div className="object-item__name">{object_item.name}</div>
                        <div className="object-item__assets-count">has {object_item.assets_count} assets</div>
                        <div className="object-item__tags">
                            {object_item.hashtags.map((tag_item, i) => this.renderHashTag(tag_item,i+1))}
                            {this.renderHashTagMore(object_item.hashtags.length)}
                        </div>
                        <div className="object-item__status">Active</div>
                    </div>
                </Link>
            </div>
        )
    }

}

ObjectCard.propTypes = {
    // object_item: PropTypes.object.isRequired,
};
export default ObjectCard;

