import React, { PropTypes } from 'react'
import { Link, NavLink } from 'react-router-dom';
import { Progress } from 'reactstrap';
import { CustomInput, Form, FormGroup, Label } from 'reactstrap';
import rocket from '../images/documents-sidebar/rocket-icon.svg'
import folder from "../images/files-icon/folder-icon.svg";
import ReactSVG from "react-svg";

class SidebarDocuments extends React.Component {

    render(){

        return(
            <div>
                <div className="sidebar-documents">
                    {/*Sidebar header*/}
                    <div className="media documents-header">
                        {/*Icon rocket*/}
                        <div className="documents-header__icon">
                            <img src={rocket} className="documents-header__icon-img" alt="alert"/>
                        </div>
                        {/*Progress bar files*/}
                        <div className="media-body documents-header__content">
                            <h5 className="documents-header__title">ooto Space</h5>
                            <div className="documents-header__progress-bar">
                                <Progress className="documents-header__content-progress" value={8.08} max={40.53} />
                                <div className="documents-header__content-text">8.08GB / 40.53GB</div>
                            </div>
                        </div>
                    </div>
                    {/*Sidebar content*/}
                    <div className="accordion documents-wrap" id="documents">
                        {/*File details*/}
                        <div className="card documents-item">
                            <div className="card-header documents-item__header" id="fileDetails">
                                <h5 className="documents-item__header-text">
                                    <button className="btn documents-item__header-btn collapsed" type="button" data-toggle="collapse" data-target="#collapsefileDetails" aria-expanded="true" aria-controls="collapsefileDetails">
                                        <span className="documents-title">File details</span>
                                        <i className="fas fa-chevron-down documents-icon"/>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsefileDetails" className="collapse documents-item__content" aria-labelledby="fileDetails" data-parent="#documents">
                                <div className="card-body documents-item__content-body">
                                    <div className="custom-control custom-switch documents-switch">
                                        <input type="checkbox" className="custom-control-input documents-switch__input" id="fileSwitch1"/>
                                        <label className="custom-control-label documents-switch__label" htmlFor="fileSwitch1">File Sharing</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/*Settings*/}
                        <div className="card documents-item">
                            <div className="card-header documents-item__header" id="settingsDoc">
                                <h5 className="documents-item__header-text">
                                    <button className="btn documents-item__header-btn collapsed" type="button" data-toggle="collapse" data-target="#collapsesettingsDoc" aria-expanded="true" aria-controls="collapsesettingsDoc">
                                        <span className="documents-title">Settings</span>
                                        <i className="fas fa-chevron-down documents-icon"/>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsesettingsDoc" className="collapse documents-settings__content" aria-labelledby="settingsDoc" data-parent="#documents">
                                <div className="card-body documents-settings__content-body">
                                    <div className="custom-control custom-switch documents-switch">
                                        <input type="checkbox" className="custom-control-input documents-switch__input" id="settingSwitch1"/>
                                        <label className="custom-control-label documents-switch__label" htmlFor="settingSwitch1">File Sharing</label>
                                        <span className="documents-switch__label-text">Turn on to share your file changes and updates</span>
                                    </div>
                                    <div className="custom-control custom-switch documents-switch">
                                        <input type="checkbox" className="custom-control-input documents-switch__input" id="settingSwitch2"/>
                                        <label className="custom-control-label documents-switch__label" htmlFor="settingSwitch2">Backup</label>
                                    </div>
                                    <div className="custom-control custom-switch documents-switch">
                                        <input type="checkbox" className="custom-control-input documents-switch__input" id="settingSwitch3"/>
                                        <label className="custom-control-label documents-switch__label" htmlFor="settingSwitch3">Dropbox Sync</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/*Recent files*/}
                        <div className="card documents-item">
                            <div className="card-header documents-item__header" id="recentFiles">
                                <h5 className="documents-item__header-text">
                                    <button className="btn btn documents-item__header-btn collapsed" type="button" data-toggle="collapse" data-target="#collapserecentFiles" aria-expanded="true" aria-controls="collapserecentFiles">
                                        <span className="documents-title">Recent files</span>
                                        <i className="fas fa-chevron-down documents-icon"/>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapserecentFiles" className="collapse documents-recent__content" aria-labelledby="recentFiles" data-parent="#documents">
                                <ul className="card-body documents-recent__content-body">
                                    <li className="recent-content">
                                        <span className="recent-content__title">files
                                            <span className="recent-content__title-time">A moment ago</span>
                                        </span>
                                        <ul className="recent-content__file">
                                            <li className="recent-content__file-item">
                                                <Link to={"/"} className="file-item__link">
                                                    <span className="file-link__icon file-link__icon-pptx">
                                                        <span className="file-link__text-pptx">.pptx</span>
                                                    </span>
                                                    <span className="file-link__text">Content_Client_Presentation.pptx</span>
                                                </Link>
                                            </li>
                                            <li className="recent-content__file-item">
                                                <Link to={"/"} className="file-item__link">
                                                    <span className="file-link__icon file-link__icon-pdf">
                                                        <span className="file-link__text-pdf">.pdf</span>
                                                    </span>
                                                    <span className="file-link__text">Hikoot_Proposal.pdf</span>
                                                </Link>
                                            </li>
                                            <li className="recent-content__file-item">
                                                <Link to={"/"} className="file-item__link">
                                                    <span className="file-link__icon file-link__icon-jpeg">
                                                        <span className="file-link__text-jpeg">.jpeg</span>
                                                    </span>
                                                    <span className="file-link__text">Client_Sketches.jpeg</span>
                                                </Link>
                                            </li>
                                        </ul>
                                    </li>
                                    <li className="recent-content">
                                        <span className="recent-content__title">Samuel Spencer shared a folder with you
                                            <span className="recent-content__title-time">1 hour ago</span>
                                        </span>
                                        <div className="recent-content__folder">
                                            <Link to={"/"} className="folder-item__link">
                                                <ReactSVG className="folder-link__icon" src={folder} alt=""/>
                                                <span className="folder-link__text">Inspiration</span>
                                            </Link>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

SidebarDocuments.propTypes = {
    // object_item: PropTypes.object.isRequired,
};
export default SidebarDocuments;

