import React, { PropTypes } from 'react'
import { Link, NavLink } from 'react-router-dom';
import CalendarSide from "../components/CalendarSide";

class SidebarMeetings extends React.Component {

    render(){

        return(
            <div>
                <div className="media calendar-item">
                    <i className="far fa-calendar calendar-item__icon"/>
                    <div className="media-body calendar-item__content">
                        <h5 className="calendar-item__content-title">Calendar</h5>
                        <span className="calendar-item__content-subtitle"><span className="calendar-item__subtitle-number">02</span>March</span>
                    </div>
                </div>
                <div className="calendar-sidebar">
                    <CalendarSide/>
                </div>
                <div className="accounts-item">
                    <span className="accounts-item__name">ACCOUNTS</span>
                    <ul className="accounts-item__scroll">
                        <li className="dot-style accounts-item__scroll-one">adrian.madacs@gmail.com</li>
                        <li className="dot-style accounts-item__scroll-two">ama@umwelt.dk</li>
                    </ul>
                    <Link className="btn accounts-item__btn" to={"/"} role="button">+ Add Account</Link>
                </div>
            </div>
        )
    }

}

SidebarMeetings.propTypes = {
    // object_item: PropTypes.object.isRequired,
};
export default SidebarMeetings;

