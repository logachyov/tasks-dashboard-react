import React, { PropTypes } from 'react'
import { Link, NavLink } from 'react-router-dom';
import SidebarSessions from "../components/SidebarSessions";
import SidebarMeetings from "../components/SidebarMeetings";
import SidebarDocuments from "../components/SidebarDocuments";
import SidebarUploader from "../components/SidebarUploader";


class Sidebar extends React.Component {

    render(){

        return(
            <div>
                <div className="sidebar">
                    {/*<SidebarSessions/>*/}
                    {/*<SidebarMeetings/>*/}
                    {/*<SidebarDocuments/>*/}
                    <SidebarUploader/>
                </div>
            </div>
        )
    }

}

Sidebar.propTypes = {
    // object_item: PropTypes.object.isRequired,
};
export default Sidebar;

