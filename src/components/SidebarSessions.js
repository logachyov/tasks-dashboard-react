import React, { PropTypes } from 'react'
import { Link, NavLink } from 'react-router-dom';
import bell from '../images/sessions-sidebar/bell.svg'
import expired from '../images/sessions-sidebar/expired.svg'

class SidebarSessions extends React.Component {

    render(){

        return(
            <div>
                <div className="sidebar-sessions">
                    <div className="media allert-item">
                        <img src={bell} className="allert-item__icon" alt="alert"/>
                        <span className="allert-item__number">3</span>
                        <div className="media-body allert-item__content">
                            <h5 className="allert-item__content-title">Forecast</h5>
                            <span className="allert-item__content-subtitle">14 February</span>
                        </div>
                    </div>
                    <div className="accordion tasks-wrap" id="tasks">
                        <div className="card tasks-item">
                            <div className="card-header tasks-item__header" id="tasksOne">
                                <h5 className="tasks-item__header-text">
                                    <button className="btn tasks-item__header-btn collapsed" type="button" data-toggle="collapse" data-target="#collapsetasksOne" aria-expanded="true" aria-controls="collapsetasksOne">
                                        <span className="tasks-item__header-time">9:45</span>
                                        <span className="tasks-title header-done">Reply to Debie Wilhelm</span>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsetasksOne" className="collapse tasks-item__content" aria-labelledby="tasksOne" data-parent="#tasks">
                                <div className="card-body tasks-item__content-body">
                                    <div className="custom-control custom-checkbox content-body__btn">
                                        <input type="checkbox" className="custom-control-input" id="tasksCheck1"/>
                                        <label className="custom-control-label content-body__btn-text" htmlFor="tasksCheck1">Accept</label>
                                    </div>
                                    <Link to={"/"} className="content-body__view">View Event</Link>
                                </div>
                            </div>
                        </div>
                        <div className="card tasks-item">
                            <div className="card-header tasks-item__header" id="tasksTwo">
                                <h5 className="tasks-item__header-text">
                                    <button className="btn tasks-item__header-btn collapsed" type="button" data-toggle="collapse" data-target="#collapsetasksTwo" aria-expanded="true" aria-controls="collapsetasksTwo">
                                        <span className="tasks-item__header-time">9:59</span>
                                        <span className="tasks-title">
                                            <img className="tasks-title__expired" src={expired} alt="expired"/>New invitation from Julie
                                        </span>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsetasksTwo" className="collapse tasks-item__content" aria-labelledby="tasksTwo" data-parent="#tasks">
                                <div className="card-body tasks-item__content-body">
                                    <div className="custom-control custom-checkbox content-body__btn">
                                        <input type="checkbox" className="custom-control-input" id="tasksCheck2"/>
                                        <label className="custom-control-label content-body__btn-text" htmlFor="tasksCheck2">Accept</label>
                                    </div>
                                    <Link to={"/"} className="content-body__view">View Event</Link>
                                </div>
                            </div>
                        </div>
                        <div className="card tasks-item">
                            <div className="card-header tasks-item__header" id="tasksThree">
                                <h5 className="tasks-item__header-text">
                                    <button className="btn btn tasks-item__header-btn collapsed" type="button" data-toggle="collapse" data-target="#collapsetasksThree" aria-expanded="true" aria-controls="collapsetasksThree">
                                        <span className="tasks-item__header-time">10:30</span>
                                        <span className="tasks-title">Wireframes with Josh</span>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsetasksThree" className="collapse tasks-item__content" aria-labelledby="tasksThree" data-parent="#tasks">
                                <div className="card-body tasks-item__content-body">
                                    <div className="custom-control custom-checkbox content-body__btn">
                                        <input type="checkbox" className="custom-control-input" id="tasksCheck3"/>
                                        <label className="custom-control-label content-body__btn-text" htmlFor="tasksCheck3">Accept</label>
                                    </div>
                                    <Link to={"/"} className="content-body__view">View Event</Link>
                                </div>
                            </div>
                        </div>
                        <div className="card tasks-item">
                            <div className="card-header tasks-item__header" id="tasksFour">
                                <h5 className="tasks-item__header-text">
                                    <button className="btn tasks-item__header-btn collapsed" type="button" data-toggle="collapse" data-target="#collapsetasksFour" aria-expanded="true" aria-controls="collapsetasksFour">
                                        <span className="tasks-item__header-time">12:45</span>
                                        <span className="tasks-title">Follow up on Wireframes and Design</span>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsetasksFour" className="collapse tasks-item__content" aria-labelledby="tasksFour" data-parent="#tasks">
                                <div className="card-body tasks-item__content-body">
                                    <div className="custom-control custom-checkbox content-body__btn">
                                        <input type="checkbox" className="custom-control-input" id="tasksCheck4"/>
                                        <label className="custom-control-label content-body__btn-text" htmlFor="tasksCheck4">Accept</label>
                                    </div>
                                    <Link to={"/"} className="content-body__view">View Event</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

SidebarSessions.propTypes = {
    // object_item: PropTypes.object.isRequired,
};
export default SidebarSessions;

