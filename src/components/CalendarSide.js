import React, { Component } from "react";
import Calendar from "rc-calendar";
import DatePicker from 'rc-calendar/lib/Picker';

import "rc-calendar/assets/index.css";
import "../styles/components/calendarside.scss";
import moment from "moment";
import "moment/locale/en-gb";

class CalendarSide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: moment(),
            open: false,
            showDateFormat: moment().format("MMMM YYYY"),
        }

    }

    formatter=(showDateFormat) => {
        this.setState({
            showDateFormat,
        });
    }
    heandlePicker=(open) => {
        this.setState({
            open,
        });
    }
    onChange=(value) => {
        this.setState({
            value,
        });
    }
    render() {
        let {showInput} = this.props;
        let isHidden = showInput ? "" : "hidden";

        const calendar = (
            <Calendar
                defaultValue={moment()}
                showOk={false}
                showToday={false}
                format={this.format}
                showDateInput={false}
            />
        );
        return (
            <div>
                <DatePicker
                    animation="slide-up"
                    value={this.state.value}
                    calendar={calendar}
                    open={!(showInput && !this.state.open)}
                    onOpenChange={this.heandlePicker}
                    onChange={this.onChange}
                >
                    {({ value }) => {
                        return <div className="calendar-style">
                            <input className={`calendar-input calendar-input__font ${isHidden}`} value={value ? value.format("D MMMM YYYY") : ""} />
                            <i className="far fa-calendar-alt calendar-style__icon-img"/>
                        </div>;
                    }}
                </DatePicker>
            </div>
        )
    }
}

export default CalendarSide;


